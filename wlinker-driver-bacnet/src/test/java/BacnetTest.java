import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.log.StaticLog;
import cn.wlinker.driver.bacnet.BacnetDriver;
import cn.wlinker.driver.bacnet.domain.BacnetBaseBean;
import cn.wlinker.driver.bacnet.domain.BacnetLocalDeviceBean;
import cn.wlinker.driver.bacnet.domain.BacnetObject;
import cn.wlinker.driver.common.DriverFactory;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * @author gxsjx
 * @version 1.0
 * @date 2022/9/22
 */
public class BacnetTest {
    static BacnetDriver bacnetDriver;

    static {
        BacnetLocalDeviceBean bacnetLocalDeviceBean = new BacnetLocalDeviceBean();
        bacnetLocalDeviceBean.setLocalBindAddress("192.168.61.1");
        bacnetLocalDeviceBean.setBroadcastAddress("255.255.255.255");
        bacnetLocalDeviceBean.setNetworkPrefixLength(24);
        Map<String, Object> stringObjectMap = BeanUtil.beanToMap(bacnetLocalDeviceBean);
        try {
            bacnetDriver = DriverFactory.getDriverByConnInfo(BacnetDriver.class,stringObjectMap);
            bacnetDriver.open();
            Thread.sleep(3000);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Test
    public void testDiscovery() throws Exception {
        Boolean opened = bacnetDriver.isOpen();
        if (!opened) {
            System.err.println("驱动已关闭");
        }
        DateTime date = DateUtil.date();
        List<BacnetObject> discovery = bacnetDriver.discovery();
        DateTime date1 = DateUtil.date();
        StaticLog.warn("开始：{}，结束：{}，耗时：{}ms", date, date1, DateUtil.betweenMs(date, date1));
        for (BacnetObject bacnetObject : discovery) {
            System.err.println(bacnetObject);
        }
    }

    @Test
    public void testRead() throws Exception {
        Boolean opened = bacnetDriver.isOpen();
        if (!opened) {
            System.err.println("驱动已关闭");
        }
        DateTime date = DateUtil.date();
        BacnetBaseBean bacnetBaseBean = new BacnetBaseBean();
        bacnetBaseBean.setRemoteId("3588238");
        bacnetBaseBean.setObjectId("AnalogInput1");
        BacnetObject read = bacnetDriver.read(bacnetBaseBean);
        DateTime date1 = DateUtil.date();
        StaticLog.warn("开始：{}，结束：{}，耗时：{}ms,\r\n {}", date, date1, DateUtil.betweenMs(date, date1), read);
    }
}

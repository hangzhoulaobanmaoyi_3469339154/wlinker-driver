package cn.wlinker.driver.bacnet.constant;


import lombok.Getter;

/**
 * 是否活跃枚举类
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/9/29
 * Copyright © wlinker.cn
 */
@Getter
public enum ActiveEnum {
    //不活跃的
    INACTIVE("0"),
    //活跃的
    ACTIVE("1");

    private final String value;

    ActiveEnum(String value) {
        this.value = value;
    }
}

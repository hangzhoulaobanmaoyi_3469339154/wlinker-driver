package cn.wlinker.driver.bacnet.domain;

/**
 * bacnet请求对象
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/9/18
 * Copyright © wlinker.cn
 */
@lombok.Data
public class BacnetBaseBean {
    /**
     * 设备id
     * example = "1234",required = true
     */
    String remoteId;

    /**
     * 属性id"
     * example = "AnalogInput10092",required = true
     */
    String objectId;

    /**
     * 属性值
     * example = "1",required = true
     */
    String value;

    /**
     * 优先级,范围1~16,数字越小优先级越高,默认16,非必要请勿设置此选项
     * example = "16",required = false
     */
    Integer priority;
}
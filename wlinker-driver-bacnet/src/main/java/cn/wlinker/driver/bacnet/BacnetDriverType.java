package cn.wlinker.driver.bacnet;

import cn.wlinker.driver.common.IDriverType;

/**
 * 楼宇自控驱动类
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/9/22
 * Copyright © wlinker.cn
 */
public class BacnetDriverType implements IDriverType<BacnetDriver> {

    public static final BacnetDriverType INSTANCE = new BacnetDriverType();

    private BacnetDriverType() {
    }

    @Override
    public String getType() {
        return "bacnet";
    }

    @Override
    public String getName() {
        return "楼宇自控协议";
    }

    @Override
    public Class<BacnetDriver> getDriver() {
        return BacnetDriver.class;
    }
}

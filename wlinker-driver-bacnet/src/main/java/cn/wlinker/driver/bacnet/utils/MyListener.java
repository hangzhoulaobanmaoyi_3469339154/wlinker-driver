package cn.wlinker.driver.bacnet.utils;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.event.DeviceEventAdapter;
import lombok.Setter;
import lombok.SneakyThrows;

/**
 * 自定义监听器，以实时添加新设备到缓存
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/9/29
 * Copyright © wlinker.cn
 */
public class MyListener extends DeviceEventAdapter {

        @Setter
        private LocalDevice localDevice;

        @SneakyThrows
        @Override
        public void iAmReceived(RemoteDevice d) {
            System.out.println("-----IAm received：" + d);
        }
    }
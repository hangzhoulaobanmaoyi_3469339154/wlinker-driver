import cn.hutool.core.bean.BeanUtil;
import cn.wlinker.driver.common.DriverFactory;
import com.wlinker.driver.opc.ua.domain.OpcUaBean;
import com.wlinker.driver.opc.ua.domain.OpcUaSimpleRes;
import com.wlinker.driver.opc.ua.utils.OpcUaConnectHelper;
import com.wlinker.driver.opc.ua.OpcUaDriver;
import com.wlinker.driver.opc.ua.domain.OpcUaConnectBean;
import com.wlinker.driver.opc.ua.domain.OpcUaRes;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UShort;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * ocp ua 测试
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/7/13
 * Copyright © wlinker.cn
 */
public class OpcUaTest {

    static OpcUaDriver driver;

    static {
        OpcUaConnectBean opcUaConnectBean = new OpcUaConnectBean();
        opcUaConnectBean.setEndPointUrl("opc.tcp://192.168.61.129:49320");
        Map<String, Object> stringObjectMap = BeanUtil.beanToMap(opcUaConnectBean);
        driver = DriverFactory.getDriverByConnInfo(OpcUaDriver.class, stringObjectMap, OpcUaConnectHelper.getInstance());
        try {
            driver.open();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 测试点位发现
     */
    @Test
    public void testDiscovery() {
        List<OpcUaRes> discovery = driver.discovery();
        for (OpcUaRes opcUaRes : discovery) {
            System.out.println(opcUaRes);
        }
    }

    /**
     * 测试读取
     */
    @Test
    public void testRead() {
        OpcUaBean opcUaBean = new OpcUaBean();
        opcUaBean.setIdentifier("test.device.k10");
        OpcUaSimpleRes read = driver.read(opcUaBean);
        System.out.println(read);
    }

    /**
     * 测试写入
     */
    @Test
    public void testWrite() {
        OpcUaBean opcUaBean = new OpcUaBean();
        opcUaBean.setIdentifier("test.device.k10");
        opcUaBean.setValue("15");
        //此处数据类型必须与设备发现中的数据类型一致
        opcUaBean.setDataType("Short");
        Boolean write = driver.write(opcUaBean);
        System.out.println(write);
    }

    /**
     * 测试写入 无符号类型
     */
    @Test
    public void testWrite2() {
        OpcUaBean opcUaBean = new OpcUaBean();
        opcUaBean.setIdentifier("通道 1.设备 1.标记 2");
        opcUaBean.setValue("15");
        //此处数据类型必须与设备发现中的数据类型一致
        opcUaBean.setDataType("UShort");
        Boolean write = driver.write(opcUaBean);
        System.out.println(write);
    }

}

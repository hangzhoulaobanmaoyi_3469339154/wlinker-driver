package com.wlinker.driver.opc.ua.constant;

import cn.wlinker.driver.common.utils.IDataTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UByte;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UInteger;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.ULong;
import org.eclipse.milo.opcua.stack.core.types.builtin.unsigned.UShort;

import java.lang.reflect.Method;

/**
 * 无符号数据类型
 */
@AllArgsConstructor
@Getter
public enum UnsignedDataTypeEnum implements IDataTypeEnum {
    U_BYTE(UByte.class),
    U_SHORT(UShort.class),
    U_INTEGER(UInteger.class),
    U_LONG(ULong.class),
    ;
    private Class<?> clazz;

    @SneakyThrows
    @Override
    public Object toTargetValue(Object value) {
        Method valueOf = getClazz().getMethod("valueOf", String.class);
        return valueOf.invoke(null, value.toString());
    }
}

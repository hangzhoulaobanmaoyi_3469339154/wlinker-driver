package com.wlinker.driver.opc.ua.domain;

/**
 * opc ua连接类
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/7/13
 * Copyright © wlinker.cn
 */
@lombok.Data
public class OpcUaConnectBean {

    /**
     * 连接地址
     */
    private String endPointUrl;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;
}

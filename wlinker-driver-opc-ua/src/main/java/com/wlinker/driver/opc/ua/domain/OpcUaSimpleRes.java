package com.wlinker.driver.opc.ua.domain;

import lombok.Data;

/**
 * opc ua响应类
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/7/13
 * Copyright © wlinker.cn
 */
@Data
public class OpcUaSimpleRes {

    /**
     * 命名空间
     */
    private String identifier;

    /**
     * 属性值
     */
    private Object value;

    /**
     * 是否健康
     */
    private Boolean isGood;
}

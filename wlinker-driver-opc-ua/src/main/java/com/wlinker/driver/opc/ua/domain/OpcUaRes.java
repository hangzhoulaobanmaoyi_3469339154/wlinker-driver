package com.wlinker.driver.opc.ua.domain;

import cn.wlinker.driver.common.utils.DataTypeEnumSupplier;
import lombok.Data;

import java.util.Date;

/**
 * opc ua响应类
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/7/13
 * Copyright © wlinker.cn
 */
@Data
public class OpcUaRes {
    /**
     * 命名空间
     */
    private Integer namespace;

    /**
     * 标识符
     */
    private String identifier;

    /**
     * 名称 displayName
     */
    private String name;

    /**
     * 属性值
     */
    private Object value;

    /**
     * 时间戳
     */
    private Date sourceTime;

    /**
     * @see DataTypeEnumSupplier
     * 数据类型
     */
    private String dataType;

    /**
     * 点位是否健康
     */
    private Boolean isGood;
}

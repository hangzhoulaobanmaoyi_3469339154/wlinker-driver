package com.wlinker.driver.opc.ua.domain;

import cn.wlinker.driver.common.utils.DataTypeEnumSupplier;

/**
 * opc ua传输类
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/7/13
 * Copyright © wlinker.cn
 */
@lombok.Data
public class OpcUaBean {
    /**
     * 命名空间
     */
    private Integer namespace;

    /**
     * 标识符
     */
    private String identifier;

    /**
     * @see DataTypeEnumSupplier
     * 数据类型
     */
    private String dataType;

    /**
     * 属性值
     */
    private String value;
}

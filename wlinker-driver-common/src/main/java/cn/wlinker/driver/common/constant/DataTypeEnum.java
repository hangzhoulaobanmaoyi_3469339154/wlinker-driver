package cn.wlinker.driver.common.constant;

import cn.hutool.core.convert.Convert;
import cn.wlinker.driver.common.utils.IDataTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 数据类型枚举
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/7/14
 * Copyright © wlinker.cn
 */
@AllArgsConstructor
@Getter
public enum DataTypeEnum implements IDataTypeEnum {
    INTEGER(Integer.class),
    LONG(Long.class),
    DOUBLE(Double.class),
    FLOAT(Float.class),
    CHARACTER(Character.class),
    BOOLEAN(Boolean.class),
    BYTE(Byte.class),
    SHORT(Short.class),
    STRING(String.class),
    ;
    private Class<?> clazz;

    @Override
    public Object toTargetValue(Object value) {
       return Convert.convert(getClazz(), value);
    }

}

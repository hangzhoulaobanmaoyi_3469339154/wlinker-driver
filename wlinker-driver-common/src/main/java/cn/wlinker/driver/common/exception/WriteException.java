
package cn.wlinker.driver.common.exception;

/**
 * 写入异常
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/4/25
 * Copyright © wlinker.cn
 */
public class WriteException extends DriverException {
    public WriteException(String message) {
        super(message);
    }

    public WriteException(String message, Throwable cause) {
        super(message, cause);
    }

    public WriteException(Throwable cause) {
        super(cause);
    }

    public WriteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

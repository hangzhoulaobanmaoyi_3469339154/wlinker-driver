package cn.wlinker.driver.common;

import cn.wlinker.driver.common.utils.IConnectHelper;
import cn.wlinker.driver.common.exception.NotSupportException;

import java.util.LinkedList;
import java.util.List;

/**
 * 通用驱动接口
 * C 连接信息对象
 * T 传输对象
 * R 查询响应对象
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/9/15
 * Copyright © wlinker.cn
 */
public interface IDriver<C, T, R> {

    IConnectHelper getConnectHelper();

    /**
     * 获取连接对象
     *
     * @return 连接对象
     */
    default C getConnectionBean() {
        throw new NotSupportException("暂不支持");
    }

    /**
     * 建立连接
     *
     * @return 是否成功
     */
    default Boolean open() throws Exception {
        throw new NotSupportException("暂不支持");
    }

    /**
     * 断开连接
     *
     * @return 是否成功
     */
    default Boolean close() {
        throw new NotSupportException("暂不支持");
    }

    /**
     * 检查是否健康
     *
     * @return 是否健康
     */
     Boolean isOpen();

    /**
     * 发现设备
     *
     * @return 设备列表
     */
    default <M> M discovery() {
        throw new NotSupportException("暂不支持");
    }

    /**
     * 发送读消息
     *
     * @param dto 读消息参数
     */
    default R read(T dto) {
        throw new NotSupportException("暂不支持");
    }

    /**
     * 批量发送读消息
     * @param dtoList 读消息参数列表
     * @return  读消息响应列表
     */
    default List<R> batchRead(List<T> dtoList) {
        List<R> res = new LinkedList<>();
        for (T t : dtoList) {
            R read = read(t);
            res.add(read);
        }
        return res;
    }

    /**
     * 发送写消息
     *
     * @param dto 写消息参数
     * @return
     */
    default Boolean write(T dto) {
        throw new NotSupportException("暂不支持");
    }

    /**
     * 批量发送写消息
     *
     * @param dtoList 写消息参数列表
     * @return 写消息响应结果列表
     */
    default List<Boolean> batchWrite(List<T> dtoList) {
        List<Boolean> res = new LinkedList<>();
        for (T t : dtoList) {
            Boolean write = write(t);
            res.add(write);
        }
        return res;
    }
}
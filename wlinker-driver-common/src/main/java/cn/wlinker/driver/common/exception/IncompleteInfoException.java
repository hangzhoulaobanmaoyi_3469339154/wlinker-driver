package cn.wlinker.driver.common.exception;

/**
 * 信息不完整异常
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/7/14
 * Copyright © wlinker.cn
 */
public class IncompleteInfoException extends DriverException {
    public IncompleteInfoException(String message) {
        super(message);
    }

    public IncompleteInfoException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncompleteInfoException(Throwable cause) {
        super(cause);
    }

    public IncompleteInfoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

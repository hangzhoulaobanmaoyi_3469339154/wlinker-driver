package cn.wlinker.driver.common.utils;

import cn.wlinker.driver.common.constant.DataTypeEnum;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 数据类型提供者
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/7/14
 * Copyright © wlinker.cn
 */
public class DataTypeEnumSupplier {

    /**
     * 数据类型缓存
     * key: 数据类型对应的字节码文件的SimpleName
     * value: 数据类型包装类
     */
    private static final Map<String, IDataTypeEnum> wrapperTypeMap = new ConcurrentHashMap<>(8);

    /**
     * 注册数据类型
     */
    static {
        DataTypeEnum[] values = DataTypeEnum.values();
        register(values);
    }

    /**
     * 批量注册数据类型
     * @param wrapperTypes
     */
    public static void register(IDataTypeEnum[] wrapperTypes){
        for (IDataTypeEnum wrapperType : wrapperTypes) {
            register(wrapperType);
        }
    }

    /**
     * 注册数据类型
     * @param wrapperType
     */
    public static void register(IDataTypeEnum wrapperType){
        wrapperTypeMap.put(wrapperType.getDataType(), wrapperType);
    }

    /**
     * 通过类型关键字获取数据类型
     * @param dataType
     * @return
     */
    public static IDataTypeEnum getOne(String dataType){
        return wrapperTypeMap.get(dataType);
    }

    /**
     * 通过类型关键字获取数据类型
     * @param dataType
     * @return
     */
    public static IDataTypeEnum getOneByTypeCode(String dataType){
        for (Map.Entry<String, IDataTypeEnum> entry : wrapperTypeMap.entrySet()) {
            IDataTypeEnum wrapperType = entry.getValue();
            if (wrapperType.getClazz().getName().equals(dataType)) {
                return wrapperType;
            }
        }
        return null;
    }
}

package cn.wlinker.driver.common.exception;

/**
 * 驱动初始化异常
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/4/25
 * Copyright © wlinker.cn
 */
public class DriverInitException extends RuntimeException{

    public DriverInitException(String message) {
        super(message);
    }

    public DriverInitException(String message, Throwable cause) {
        super(message, cause);
    }

    public DriverInitException(Throwable cause) {
        super(cause);
    }

    public DriverInitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

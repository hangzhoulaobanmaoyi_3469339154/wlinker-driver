package cn.wlinker.driver.common;

import lombok.AllArgsConstructor;

import java.io.Serializable;

/**
 * 通用连接类
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/10/9
 * Copyright © wlinker.cn
 */
@lombok.Data
@AllArgsConstructor
public class CommonConnBean implements Serializable {

    /**
     * ip地址
     */
    private String ip;

    /**
     * 端口号
     */
    private Integer port;
}

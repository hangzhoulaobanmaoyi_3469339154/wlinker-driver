package cn.wlinker.driver.common.exception;

/**
 * 未找到异常
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/4/25
 * Copyright © wlinker.cn
 */
public class NotFoundException extends DriverException {
    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }

    public NotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

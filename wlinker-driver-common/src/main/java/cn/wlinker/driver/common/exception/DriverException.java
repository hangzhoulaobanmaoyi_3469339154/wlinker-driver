package cn.wlinker.driver.common.exception;

/**
 * 自定义驱动异常
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/4/25
 * Copyright © wlinker.cn
 */
public abstract class DriverException extends RuntimeException{

    public DriverException(String message) {
        super(message);
    }

    public DriverException(String message, Throwable cause) {
        super(message, cause);
    }

    public DriverException(Throwable cause) {
        super(cause);
    }

    public DriverException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

package cn.wlinker.driver.common.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 连接信息缓存工具类
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/7/13
 * Copyright © wlinker.cn
 */
public class ConnectionCacheUtils {

    /**
     * 连接信息缓存
     * key: 由连接信息生成
     * value: 连接对象
     */
    private static final Map<String, Object> connectionMap = new ConcurrentHashMap<>();

    /**
     * 添加连接缓存
     * @param key
     * @param value
     */
    public static void put(String key, Object value) {
        connectionMap.put(key, value);
    }

    /**
     * 获取连接缓存
     * @param key
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> T get(String key, Class<T> cls) {
       return cls.cast(connectionMap.get(key));
    }

    /**
     * 获取连接缓存
     * @param key
     * @param <T>
     * @return
     */
    public static <T> T get(String key) {
        return (T) connectionMap.get(key);
    }

    /**
     * 删除连接缓存
     * @param key
     */
    public static void remove(String key) {
        connectionMap.remove(key);
    }
}


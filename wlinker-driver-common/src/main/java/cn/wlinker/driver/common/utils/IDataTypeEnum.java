package cn.wlinker.driver.common.utils;

/**
 * 数据类型接口
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/7/14
 * Copyright © wlinker.cn
 */
public interface IDataTypeEnum {

    /**
     * 获取数据类型
     * @return
     */
    default String getDataType(){
        return getClazz().getSimpleName();
    }

    /**
     * 获取数据类型对应的字节码文件
     * @return
     */
    Class<?> getClazz();

    /**
     * 将Object对象转换为指定的java数据类型
     * @param value
     * @return
     */
    Object toTargetValue(Object value);

    /**
     * 获取Object对象对应的java数据类型的字符串
     */
    static String getDataType(Object value) {
        IDataTypeEnum oneByClass = DataTypeEnumSupplier.getOneByTypeCode(value.getClass().getName());
        if (oneByClass != null) {
            return oneByClass.getDataType();
        }
        return null;
    }

    /**
     * 将Object对象转换为指定的java数据类型
     * @param targetDataType
     * @param value
     * @return
     */
    static Object toTargetValue(String targetDataType, Object value) {
        IDataTypeEnum one = DataTypeEnumSupplier.getOne(targetDataType);
        if (one != null) {
            return one.toTargetValue(value);
        }
        return null;
    }
}

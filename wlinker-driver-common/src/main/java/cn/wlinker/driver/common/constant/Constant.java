package cn.wlinker.driver.common.constant;

/**
 * 常量类定义
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/9/18
 * Copyright © wlinker.cn
 */
public class Constant {

    /**
     * 重试次数
     */
    public static final int RETRIES = 5;

    /**
     * 超时时长 单位ms
     */
    public static final int TIMEOUT_MS = 1000;

}

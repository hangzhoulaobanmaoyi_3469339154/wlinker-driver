
package cn.wlinker.driver.common.exception;

/**
 * 读取异常
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/4/25
 * Copyright © wlinker.cn
 */
public class ReadException extends DriverException {
    public ReadException(String message) {
        super(message);
    }

    public ReadException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReadException(Throwable cause) {
        super(cause);
    }

    public ReadException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

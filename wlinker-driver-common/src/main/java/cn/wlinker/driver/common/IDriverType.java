package cn.wlinker.driver.common;

import cn.wlinker.driver.common.exception.NotFoundException;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 驱动类别接口
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/9/22
 * Copyright © wlinker.cn
 */
public interface IDriverType<T extends AbstractDriver> {
    
    Map<String, IDriverType<?>> suppliers = new ConcurrentHashMap<>(8);
    
    String getType();

    String getName();
    
    Class<T> getDriver();
    
    static void register(IDriverType<?> protocolTypeEnum){
        suppliers.put(protocolTypeEnum.getType(), protocolTypeEnum);
    }

    static void register(Collection<IDriverType<?>> protocolTypeEnums){
        protocolTypeEnums.forEach(IDriverType::register);
    }

    static IDriverType getOne(String type) {
        if(suppliers.containsKey(type)){
            return suppliers.get(type);
        }
        throw new NotFoundException("没有找到对应的协议");
    }
}
import cn.wlinker.driver.common.utils.IDataTypeEnum;
import org.junit.Test;

/**
 * 数据类型测试
 *
 * @author gxsjx
 * @version 1.0
 * @date 2023/7/14
 * Copyright © wlinker.cn
 */
public class DataTypeTest {

    @Test
    public void testOne(){
        System.out.println(IDataTypeEnum.getDataType(1));
        System.out.println(IDataTypeEnum.getDataType(1L));
        System.out.println(IDataTypeEnum.getDataType(1.0));
        System.out.println(IDataTypeEnum.getDataType(1.0f));
        System.out.println(IDataTypeEnum.getDataType('a'));
        System.out.println(IDataTypeEnum.getDataType(true));
        System.out.println(IDataTypeEnum.getDataType((byte) 1));
        System.out.println(IDataTypeEnum.getDataType((short) 1));

        Object integer = IDataTypeEnum.toTargetValue("Integer", "1");
        System.out.println(integer.getClass().getName());
    }
}

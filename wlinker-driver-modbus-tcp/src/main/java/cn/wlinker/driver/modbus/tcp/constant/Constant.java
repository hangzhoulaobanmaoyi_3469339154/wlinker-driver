package cn.wlinker.driver.modbus.tcp.constant;

/**
 * 常量类定义
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/10/13
 * Copyright © wlinker.cn
 */
public class Constant {

    /**
     * 重试次数
     */
    public static final int RETRIES = 3;

    /**
     * 超时时长 单位ms
     */
    public static final int TIMEOUT_MS = 1000;

}

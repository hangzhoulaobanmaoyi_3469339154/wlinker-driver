package cn.wlinker.driver.modbus.tcp.domain;

import cn.wlinker.driver.common.CommonConnBean;

import java.util.List;

/**
 * modbus响应结果
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/10/12
 */
@lombok.Data
public class ModbusRes extends CommonConnBean {

    public ModbusRes(String ip, Integer port, List<ModbusResItem> resBeanList) {
        super(ip, port);
        this.list = resBeanList;
    }

    /**
     * 结果列表
     */
    private transient List<ModbusResItem> list;
}

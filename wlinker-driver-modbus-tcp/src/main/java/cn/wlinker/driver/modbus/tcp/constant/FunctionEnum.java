package cn.wlinker.driver.modbus.tcp.constant;

import com.serotonin.modbus4j.code.RegisterRange;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;

/**
 * 寄存器类型枚举
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/10/13
 * Copyright © wlinker.cn
 */
@AllArgsConstructor
@Getter
public enum FunctionEnum {

    COIL_STATUS(RegisterRange.COIL_STATUS, "输出线圈", "01", false, "05", "15"),
    INPUT_STATUS(RegisterRange.INPUT_STATUS, "输入线圈", "02", true),
    HOLDING_REGISTER(RegisterRange.HOLDING_REGISTER, "保持寄存器", "03", false, "06", "16"),
    INPUT_REGISTER(RegisterRange.INPUT_REGISTER, "输入寄存器", "04", false),
    ;

    FunctionEnum(Integer type, String typeName, String readFunctionCode, Boolean readOnly) {
        this.type = type;
        this.typeName = typeName;
        this.readFunctionCode = readFunctionCode;
        this.readOnly = readOnly;
    }


    private final Integer type;
    private final String typeName;
    private final String readFunctionCode;
    private final Boolean readOnly;
    private String writeFunctionCode;
    private String batchWriteFunctionCode;


    public static Optional<FunctionEnum> getByType(Integer type){
        FunctionEnum[] values = FunctionEnum.values();
        for (FunctionEnum functionEnum : values) {
            boolean equals = functionEnum.getType().equals(type);
            if(equals){
                return Optional.of(functionEnum);
            }
        }
        return Optional.empty();
    }
}

package cn.wlinker.driver.modbus.tcp.domain;

import cn.wlinker.driver.modbus.tcp.constant.FunctionEnum;

/**
 * modbus响应对象
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/10/12
 */
@lombok.Data
public class ModbusResItem {
    /**
     * 从机编号
     * example = "1",required = true
     */
    private Integer slaveId ;

    /**
     * 寄存器类型
     * @see FunctionEnum
     */
    private Integer functionType;

    /**
     * 寄存器地址
     * example = "1",required = true
     */
    private Integer address;

    /**
     * 属性值
     * example = "1"
     */
    private String value;
}

package cn.wlinker.driver.modbus.tcp;

import cn.wlinker.driver.common.IDriverType;

/**
 * modbus tcp驱动类别
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/10/9
 */
public class ModbusTcpDriverType implements IDriverType<ModbusTcpDriver> {

    public static final ModbusTcpDriverType INSTANCE = new ModbusTcpDriverType();

    private ModbusTcpDriverType() {
    }

    @Override
    public String getType() {
        return "modbus-tcp";
    }

    @Override
    public String getName() {
        return "modbus tcp协议";
    }

    @Override
    public Class<ModbusTcpDriver> getDriver() {
        return ModbusTcpDriver.class;
    }
}

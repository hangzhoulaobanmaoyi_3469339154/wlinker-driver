package cn.wlinker.driver.modbus.tcp.domain;

import cn.wlinker.driver.modbus.tcp.constant.FunctionEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ModbusBean implements Serializable {

	private static final long serialVersionUID = -4352914047862112224L;

	/**
	 * 从机编号
	 * example = "1",required = true
	 */
	private Integer slaveId ;

	/**
	 * 功能类型
	 * 1  2  3  4
	 * example = "1",required = true
	 * @see FunctionEnum
	 */
	private Integer functionType;


	/**
	 * 起始地址
	 * example = "1",required = true
	 */
	private Integer address;

	/**
	 * 要读取的数据个数
	 * example = "1"
	 */
	private Integer quantity;

	/**
	 * 数据类别
	 * example = "1"
	 * @see cn.wlinker.driver.modbus.tcp.constant.DataTypeEnum
	 */
	private Integer dataType;

	/**
	 * 目标值,多个用英文逗号隔开
	 * example = "1"
	 */
	private String value;

	public static void main(String[] args) {
//		ModbusBean build = new ModbusBeanBuilder().slaveId(1).functionCode("05").registerCount(1).build();
//		String s = JSONUtil.toJsonStr(build);
//		System.err.println(s);
	}
}
import cn.hutool.core.bean.BeanUtil;
import cn.wlinker.driver.common.CommonConnBean;
import cn.wlinker.driver.common.DriverFactory;
import cn.wlinker.driver.modbus.tcp.ModbusTcpDriver;
import cn.wlinker.driver.modbus.tcp.constant.DataTypeEnum;
import cn.wlinker.driver.modbus.tcp.constant.FunctionEnum;
import cn.wlinker.driver.modbus.tcp.domain.ModbusBean;
import cn.wlinker.driver.modbus.tcp.domain.ModbusRes;
import org.junit.Test;

import java.util.Map;

/**
 * modbus测试工具
 *
 * @author gxsjx
 * @version 1.0
 * @date 2022/10/12
 */
public class ModbusTest {

    static ModbusTcpDriver modbusTcpDriver;

    static {
        CommonConnBean connBean = new CommonConnBean("127.0.0.1",502);
        Map<String, Object> stringObjectMap = BeanUtil.beanToMap(connBean);
        try {
            modbusTcpDriver = DriverFactory.getDriverByConnInfo(ModbusTcpDriver.class,stringObjectMap);
            modbusTcpDriver.open();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Test
    public void readCoil(){
        ModbusBean modbusBean  = new ModbusBean();
        modbusBean.setSlaveId(1);
        modbusBean.setFunctionType(FunctionEnum.COIL_STATUS.getType());
        modbusBean.setAddress(0);
        modbusBean.setQuantity(10);
        ModbusRes read = modbusTcpDriver.read(modbusBean);
        System.out.println(read);
    }


    @Test
    public void writeCoil(){
        ModbusBean modbusBean  = new ModbusBean();
        modbusBean.setSlaveId(1);
        modbusBean.setFunctionType(FunctionEnum.COIL_STATUS.getType());
        modbusBean.setAddress(0);
        modbusBean.setQuantity(10);
        modbusBean.setValue("1,0,1");
        Boolean write = modbusTcpDriver.write(modbusBean);
        System.out.println(write);
    }

    @Test
    public void readInputCoil(){
        ModbusBean modbusBean  = new ModbusBean();
        modbusBean.setSlaveId(1);
        modbusBean.setFunctionType(FunctionEnum.INPUT_STATUS.getType());
        modbusBean.setAddress(0);
        ModbusRes read = modbusTcpDriver.read(modbusBean);
        System.out.println(read);
    }

    @Test
    public void readHoldingRegister(){
        ModbusBean modbusBean  = new ModbusBean();
        modbusBean.setSlaveId(1);
        modbusBean.setFunctionType(FunctionEnum.HOLDING_REGISTER.getType());
        modbusBean.setAddress(0);
        modbusBean.setQuantity(3);
        modbusBean.setDataType(DataTypeEnum.DOUBLE_ABCDEFGH.getType());
        ModbusRes read = modbusTcpDriver.read(modbusBean);
        System.out.println(read);
    }

    @Test
    public void writeHoldingRegister(){
        ModbusBean modbusBean  = new ModbusBean();
        modbusBean.setSlaveId(1);
        modbusBean.setFunctionType(FunctionEnum.HOLDING_REGISTER.getType());
        modbusBean.setAddress(2);
        modbusBean.setValue("333,444,555,666");
        Boolean write = modbusTcpDriver.write(modbusBean);
        System.out.println(write);
    }

    @Test
    public void readInputRegister(){
        ModbusBean modbusBean  = new ModbusBean();
        modbusBean.setSlaveId(1);
        modbusBean.setFunctionType(FunctionEnum.INPUT_REGISTER.getType());
        modbusBean.setAddress(0);
        modbusBean.setQuantity(10);
        modbusBean.setDataType(DataTypeEnum.SIGNED.getType());
        ModbusRes read = modbusTcpDriver.read(modbusBean);
        System.out.println(read);
    }
}

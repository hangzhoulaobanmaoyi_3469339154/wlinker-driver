# wlinker-driver

#### 介绍
wlinker.cn物联网IOT各类通用驱动包,如modbus,bacnet等
此项目主要用于作为sdk被集成，目前实现的协议：

| 协议   | 设备发现（批量读取） | 读取      | 写入             |
| ------ | -------------------- | --------- | ---------------- |
| bacnet | 支持                 | 支持      | 开源版本暂不支持 |
| modbus-tcp | 支持            | 支持 | 支持       |
| opc-ua | 支持                | 支持 | 支持       |



#### 软件架构
项目技术栈：maven + java8

#### 安装教程

##### 1.打包

>在项目根目录下运行输入 mvn package 进行打包

![image-20220930193818750](images/image-20220930193818750.png)

##### 2.获取驱动jar包

> 在实际的驱动子模块的target中可以找个打包完成的jar，此jar包会自动将项目的依赖一起打包，使用时可以直接被其他项目如spring项目(J2EE)，J2SE项目依赖，无需再引入common包

![image-20220930194710621](images/image-20220930194710621.png)

#### 使用说明

##### 1.测试

> 在实际的驱动子模块的test目录中，有示例的测试代码模拟实际的使用场景
>
> 例如设备发现和单个点位读取

###### 测试代码

![image-20220930195653427](images/image-20220930195653427.png)

###### 测试结果

![image-20220930200317920](images/image-20220930200317920.png)

##### 2.被 SpringBoot+Maven项目引入

###### 拷贝jar包至resources目录

![image-20220930200740128](images/image-20220930200740128.png)

###### 在pom.xml中引入本地坐标

> 引入坐标之后需要刷新maven才能正常使用

```xml
  <dependency>
      <artifactId>wlinker-driver-bacnet</artifactId>
      <groupId>cn.wlinker</groupId>
      <version>1.0.0</version>
      <scope>system</scope>
      <systemPath>${basedir}/src/main/resources/wlinker-driver-bacnet-1.0.0.jar</systemPath>
</dependency>

```

###### 加入pom.xml打包配置

> 不加这个配置springboot项目打包时不会将本地依赖一起打包，运行时会找不到对应的Class

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <includeSystemScope>true</includeSystemScope>
            </configuration>
        </plugin>
    </plugins>
</build>
```

##### 3.接口开发

> 以下以设备发现为例

```java
@RestController
@RequestMapping("/bacnet")
@Slf4j
@Validated
public class BacnetController {


    /**
     * @param bacnetLocalDeviceBean
     * @return
     * @throws Exception
     */
    @GetMapping("/discovery")
    public List<BacnetObject> discovery(BacnetLocalDeviceBean bacnetLocalDeviceBean) throws Exception {
        Map<String, Object> stringObjectMap = BeanUtil.beanToMap(bacnetLocalDeviceBean);
        BacnetDriver bacnetDriver = DriverFactory.getDriverByConnInfo(BacnetDriver.class, stringObjectMap);
        Boolean open = bacnetDriver.open();
        if (open) {
            List<BacnetObject> discovery = bacnetDriver.discovery();
            return discovery;
        }
        return null;
    }
 }
```

##### 4.接口调用

>在接口中输入地址和需要的参数即可获取查询结果（PS:192.168.61是应用部署电脑的IP地址,需要和楼控软件在同一个网段）

![image-20220930202403051](images/image-20220930202403051.png)

##### 备注
<span style="color:red;">以下仅和Bacnet驱动测试配置相关，其他驱动测试教程后续补充</span>
> 服务应用和Bacnet设备需要在不同的机器上，而且需要在相同的网段，否则无法扫描到数据

SpringBoot应用部署地址

> SpringBoot应用部署在我本地，和虚拟机桥接用的IP地址是192.168.61.1

![image-20220930203142694](images/image-20220930203142694.png)

###### Bacnet模拟设备地址

> 设备模拟软件安装在windows虚拟机上，IP是192.168.61.129

![image-20220930203412774](images/image-20220930203412774.png)

#### 代码运行前必看

此代码基于Yabe这个可视化工具及其附带的设备模拟器进行编写。

- Yabe下载地址：[liquidtelecom.dl.sourceforge.net/project/yet…](https://link.juejin.cn/?target=https%3A%2F%2Fliquidtelecom.dl.sourceforge.net%2Fproject%2Fyetanotherbacnetexplorer%2FSetupYabe_v1.2.2.exe)

- Yabe安装后将自动带有模拟器 Bacnet.Room.Simulator.exe。（模拟器文件路径：模拟器在Yabe软件安装路径的/AddOn文件夹下。我的文件路径为：C:\Program Files\Yabe\AddOn\Bacnet.Room.Simulator.exe）

  ![image-20220930204931977](images/image-20220930204931977.png)

运行Yabe软件附带的 Bacnet.Room.Simulator.exe 软件需要和SpringBoot服务应用在不同的机器上。

运行代码或模拟器前：需关闭 Yabe、InneaBACnetExplorer Free Edition，BancetScan等所有的浏览设备信息的可视化工具，否则代码里将无法请求到设备

- tips：该模拟器支持多开（多次打开此模拟器，会启动多个设备id不同的设备）



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
